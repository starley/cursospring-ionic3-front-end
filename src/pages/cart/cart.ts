import { ProdutoDTO } from './../../models/produto.dto';
import { CartService } from './../../services/domain/cart.service';
import { ProdutoService } from './../../services/domain/produto.service';
import { CartItem } from './../../models/cart-item';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { API_CONFIG } from '../../config/api.config';

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  // Coleção de items de carrinho
  items : CartItem[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public cartService: CartService,
    public produtoService: ProdutoService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
   // Pegando o carrinho no localStorage
   let cart = this.cartService.getCart();
   this.items = cart.items;

   this.loadImageUrls();

  // console.log("Entrou no carrinho")
   
  }

  // Metodo para buscar e exibir a imagem //
  loadImageUrls(){
    for(var i = 0; i < this.items.length; i++){
      let item = this.items[i];
      this.produtoService.getSmallImageFromBucket(item.produto.id)
        .subscribe(response => {
          item.produto.imageUrl = `${API_CONFIG.bucketBaseUrl}/prod${item.produto.id}-small.jpg`;
        },
        error => {});
    }
  }

  
  // Removendo item do carrinho //
  removeItem(produto: ProdutoDTO){
  
    // Perguntando se quer remover o produto //
    let alert = this.alertCtrl.create({
      title: 'Remover produto?',
      enableBackdropDismiss: false,
      buttons: [
        {
        text: 'Sim',
        handler: () => {
          this.items = this.cartService.removeProduto(produto).items;
        }
        },
        {
        text: 'Não',
        handler: () => {
          }
          }
      ]
      
    });
    alert.present();

  }

  // Adicionando quantidade do item ao carrinho //
  increaseQuantity(produto: ProdutoDTO){
    this.items = this.cartService.increaseQuantity(produto).items;
  }

  // Removendo quantidade do item ao carrinho //
  decreaseQuantity(produto: ProdutoDTO){
    this.items = this.cartService.decreaseQuantity(produto).items;
  }

  // Somando total //
  total(): number{
    return this.cartService.total();
  }

  // Somando total de itens no carrinho //
  quatidadeItem(): number{
    return this.cartService.quatidadeItem();
  }

  // Continuar comprando //
  goOn(){
    // Retora para pagina de categorias 
    this.navCtrl.setRoot("CategoriasPage");
  }

  // Metodo para finalizar a compra //
  checkout(){

    // Verificando se existe algum item no carrinho //
    if(this.cartService.quatidadeItem() < 1){
    // Mensagem de carrinho vazio 
    const loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: "O carrinho esta vazio!",
      duration: 2000,
    });
    loader.present();

  }
  
  else {
     // Perguntando se quer finalizar a compra //
     let alert = this.alertCtrl.create({
      title: 'Finalizar compra?',
      enableBackdropDismiss: false,
      buttons: [
        {
        text: 'Sim',
        handler: () => {
          this.navCtrl.push('EnderecoPage');
        }
        },
        {
        text: 'Não',
        handler: () => {
          }
          }
      ]
      
    });
    alert.present();
  
    }
  }
}