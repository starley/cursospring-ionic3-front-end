import { API_CONFIG } from './../../config/api.config';
import { ProdutoService } from './../../services/domain/produto.service';
import { ProdutoDTO } from './../../models/produto.dto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-produtos',
  templateUrl: 'produtos.html',
})
export class ProdutosPage {

  // Inicializando uma lista vazia "[] = []" //
  items : ProdutoDTO[] = [];

  page: number = 0;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    // Declarando o produto service 
    public produtoService: ProdutoService,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
   this.loadData();
  }

  // Carregando produtos //
  loadData(){
     // Carregando produtos da categoria atravez de navegação //
     let caterogira_id = this.navParams.get('categoria_id');
     let loader = this.presentLoading();
      // Buscando 10 produtos por pagina //
     this.produtoService.findByCategoria(caterogira_id, this.page, 10)
       .subscribe(response => {
         // Variavel para pegar o tamanho da lista para colocar em uma variavel
         let start = this.items.length;
         // Concatenando a resposta //
         this.items = this.items.concat(response['content']);
         // Variavel para repoarnar o tamanho da lista já carregada 
         let end = this.items.length - 1;
         // O dismiss encerra o loading //
         loader.dismiss();
         console.log(this.page);
         console.log(this.items);
         // Chamando o metodo de imagem //
         this.loadImageUrls(start, end);
       },
       error => {
        loader.dismiss();
       });
       
  }

  // Metodo para buscar e exibir a imagem //
  loadImageUrls(start: number, end: number){
    for(var i = start; i <= end; i++){
      let item = this.items[i];
      this.produtoService.getSmallImageFromBucket(item.id)
        .subscribe(response => {
          item.imageUrl = `${API_CONFIG.bucketBaseUrl}/prod${item.id}-small.jpg`;
        },
        error => {});
    }
  }

  // Metodo para mostrar os detalhes dos produtos //
  showDetail(produto_id : string){
    this.navCtrl.push("ProdutoDetailPage", {produto_id : produto_id});
  }

  presentLoading(){
    let loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: "Aguarde!!!",

    });
    loader.present();
    return loader;

    }

    // Metodo para recarregar a pagina //
    doRefresh(refresher){
      this.page = 0;
      this.items = [];
      this.loadData();
      setTimeout(() =>{
        refresher.complete();
      }, 1000)
    }

    // Metodo para infinity Scroll
    doInfinity(infinityScroll){
      // Função para incrementar a pagaina //
      this.page++;
      // Chamando os produtos
      this.loadData();
      setTimeout(() =>{
        infinityScroll.complete();
      }, 1000);
    }

}
