import { PagamentoDTO } from './../../models/pagamento.dto';

import { CartService } from './../../services/domain/cart.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PedidoDTO } from './../../models/pedido.dto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { CartItem } from '../../models/cart-item';

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  pedido: PedidoDTO;
  // Coleção de items de carrinho
  items : CartItem[];
  numero = 1;

  // Variaveis para dados do cartão
  dataValidade="";
  cvcCard="";

  // Variaveis para mascara //
  cardNumber="";
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";
  pureResult: any;
  maskedId: any;
  val: any;
  v: any;

  // Vetor de parcelas 
  parcelas: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  formGroup: FormGroup;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public cartService: CartService) {

      // Isso ira pegar o pedido 
      this.pedido = this.navParams.get('pedido');

      this.formGroup = this.formBuilder.group({
        
        numeroDeParcelas: [1, Validators.required],
        "@type": ["pagamentoComCartao", Validators.required],
        cardNumber: [null],
        dataValidade: [null],
        cvcCard: [null]
        
      });
  }

  // Apresentado o pedido //
  nextPage(){
  
    this.pedido.pagamento = this.formGroup.value;
    this.navCtrl.setRoot('OrderConfirmationPage', {pedido: this.pedido});

  }

  // Recebendo o valor total do carrinho para ver o valor parcelados //
   total(): number{

    return this.cartService.total();

  }

  // Valor dos juros
  juros(numero: number): number{

      return this.cartService.total() * numero/100;
  }

  // Valor dos juros + valor total
  jurosTotal(numero: number): number{

    let juros = this.cartService.total() * numero/100;
    let total = this.cartService.total() + juros;

    return total;
}

  // Valor parcelado 
  totalParcela(numero: number): number{

    if(numero >= 3){
      let juros3 = this.cartService.total() / numero;
      let juros = juros3 * numero/100;
      let totals = juros3 + juros;
      return totals;
    }
    else {
      return this.cartService.total() / numero;
    }

  }

  // Criando apresentação de erro personalizado //
  presentLoading() {
    const loader = this.loadingCtrl.create({
    spinner: 'dots',
    content: "Valor digitado invalido!",
    duration: 1500,
   
  });
  loader.present();
 
}


  // Mascara para digito numero do cartão de credito //
  formatCard(valString) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.cardFormat(val).split(this.DECIMAL_SEPARATOR);
    this.pureResult = parts;
    if(parts[0].length == 16) {
      this.maskedId = this.cardMasc(parts[0]);
      return this.maskedId;
    }else{
      this.presentLoading()
      return null;
    }
  };
  
  // Limpando campo para mascara //
  cardFormat(val) {
    if (!val) {
        return '';
    }
    val = val.replace(/\D/g, '');
  
    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  };

  // Retornando o valor já mascarado //
  cardMasc(v){
  v=v.replace(/\D/g,"");
  v=v.replace(/^(\d{4})(\d)/g,"$1 $2");
  v=v.replace(/^(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3");
  v=v.replace(/^(\d{4})\s(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3 $4");
  return v;
  }

}
