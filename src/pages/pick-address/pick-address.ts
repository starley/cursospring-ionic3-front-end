import { CartService } from './../../services/domain/cart.service';
import { PedidoDTO } from './../../models/pedido.dto';
import { ClienteService } from './../../services/domain/cliente.service';
import { StorageService } from './../../services/storage.service';
import { ClienteDTO } from './../../models/cliente.dto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EnderecoDTO } from '../../models/endereco.dto';

@IonicPage()
@Component({
  selector: 'page-pick-address',
  templateUrl: 'pick-address.html',
})
export class PickAddressPage {

  items : EnderecoDTO[];
  pedido : PedidoDTO;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public storage: StorageService,
    public clienteService: ClienteService,
    public cartService: CartService) {
  }

  ionViewDidLoad() {
   // Carregando enderecos dos clientes 
   let localUser = this.storage.getLocalUser();
    if ( localUser && localUser.email){
      this.clienteService.findByEmail(localUser.email)
      .subscribe(response => {
        // Buscando os enderecos 
        this.items = response['enderecos'];

        // -- Criando o pedido -- //

        // Criando uma variavel para pegar o carrinho
        let cart = this.cartService.getCart();
        // Criando o pedido //
        this.pedido = {
          cliente: {id: response['id']},
          enderecoDeEntrega: null,
          pagamento: null,
          // Para pegar os dados dos produtos será necessario percorer a lista dos produtos escolhidos,
          // retornando a resposta desejada.
          itens: cart.items.map(x => {return {quantidade: x.quantidade, produto: {id: x.produto.id }}})
        
        }
    },
    error => {
      // Tratamento de erro 403 "Não autorizado" 
      if (error.status == 403){
        this.navCtrl.setRoot('HomePage');
      }
    });
    
  }
  // Caso não consiga obter o localUser //
  else {
    this.navCtrl.setRoot('HomePage'); 
  }
}
  // Metodo para carregar o endereco //
  nextPage(item: EnderecoDTO){
    this.pedido.enderecoDeEntrega = {id: item.id};
    this.navCtrl.push('PaymentPage', {pedido: this.pedido});


  }
}
