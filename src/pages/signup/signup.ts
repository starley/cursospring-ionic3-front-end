import { CidadeService } from './../../services/domain/cidade.service';
import { ClienteService } from './../../services/domain/cliente.service';
import { HttpClient } from '@angular/common/http';
import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EstadoService } from '../../services/domain/estado.service';
import { EstadoDTO } from '../../models/estado.dto';
import { CidadeDTO } from '../../models/cidade.dto';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  formGroup: FormGroup;

  // Variaveis de validacao //
  cpfOuCnpj = '';
  nome = '';
  telefone1 = '';
  telefone2 = '';
  telefone3 = '';
  tipo =  '';
  cep = '';
  logradouro = '';
  bairro = '';
  complemento = '';
  cidade_id = '';
  estado_id = '';
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";
  pureResult: any;
  maskedId: any;
  val: any;
  v: any;

  // Buscando o cep 
  resultado;

  // Variaveis tipo coleção //
  estados: EstadoDTO[];
  cidades: CidadeDTO[];
 
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    // Declarando os serviços //
    public cidadeService: CidadeService,
    public estadoService: EstadoService,
    public http:HttpClient,
    public clienteService: ClienteService,
    public alertCtrl: AlertController) {

      // Instanciando um formBuilder responsavel pelo group //
      this.formGroup = this.formBuilder.group(
        {
          // Fazendo a validação dos campos //
          nome:['Starley Cazorla',[Validators.required, Validators.minLength(5), Validators.maxLength(120)]],
          email:['starlley@outlook.com.br',[Validators.required, Validators.email, Validators.minLength(8), Validators.maxLength(120)]],
          // Tipo cliente já esta sendo preenchido automaticamente //
          tipo:['',[Validators.required]],
          cpfOuCnpj:['',[Validators.required, Validators.maxLength(18)]],
          senha:['123',[Validators.required]],
          logradouro:['Av',[Validators.required]],
          numero:['2',[Validators.required]],
          complemento:['Qd',[Validators.required]],
          bairro:['',[Validators.required]],
          cep:['74770-010',[Validators.required]],
          telefone1:['(62)98615600',[Validators.required]],
          telefone2:['',[]],
          telefone3:['',[]],
          estadoId : [null, [Validators.required]],
          cidadeId : [null, [Validators.required]]      
        });

  }

  // Ao iniciar a tela ira carregar automaticamente o conteudo dentro do Load() //
  ionViewDidLoad(){
   
    this.estadoService.findAll().subscribe(response =>{
      this.estados = response;
      // Pegando o primeiro elemento da lista 
      this.formGroup.controls.estadoId.setValue(this.estados[0].id);
      // Chamando o metodo de atualizar cidade // 
      this.updateCidades();
    },
    error =>{});

  }

  
  // Atualizando cidades atraves do estado //
  updateCidades(){
    // Variavel para receber o estado carregado no formulario //
    let estado_id = this.formGroup.value.estadoId;
    this.cidadeService.findAll(estado_id).subscribe(response =>{
      this.cidades = response;
      // Limpando campo de cidades //
      this.formGroup.controls.cidadeId.setValue(null);
    },
    error =>{});
  }
  
  // Enviando o forulario com os dados //
  signupUser(){ 
    let loader = this.aguardLoading()
    this.clienteService.insert(this.formGroup.value)
    .subscribe(response => {
      // Encerrando o loader 
      loader.dismiss();
      this.showInsertOk();
    },
    error =>{
      console.log("Entrou no erro!")

    });
   };
  
  // Mostrando alerta de sucesso //
  showInsertOk(){
  
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Cadastro efetuado!',
      enableBackdropDismiss: false,
      buttons: [
        {
        text: 'Ok',
        handler: () => {
          this.navCtrl.pop();
        }
        }
      ]
    });
    alert.present();

  }

  // Voltando a tela inical //
  voltarInicio(){
    
    let alert = this.alertCtrl.create({
      title: 'Ola!',
      message: 'Deseja voltar ao incio?',
      enableBackdropDismiss: false,
      buttons: [
        {
        text: 'Sim',
        handler: () => {
          this.navCtrl.setRoot("HomePage");
        }
      },
        {
          text: 'Não',
          handler: () => {
          }
        }
      ]
    });
    alert.present();

  }

  // Criando apresentação de erro personalizado //
   presentLoading() {
    const loader = this.loadingCtrl.create({
    spinner: 'dots',
    content: "Valor digitado invalido!",
    duration: 1500,
   
  });
  loader.present();
 
}

  // Criando apresentação de erro personalizado //
  aguardLoading() {
    const loader = this.loadingCtrl.create({
    spinner: 'dots',
    content: "Enviando formulario!!!",
  
  });
  loader.present();
  return loader;

  }
  
  // Validando e formatando CPF e CNPJ //
  // @autor - TomCosta //
  // Link -> https://forum.ionicframework.com/t/cpf-cnpj-input-mask/109595 //
  format(valString) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    this.pureResult = parts;
    if(parts[0].length == 11){
      this.maskedId = this.cpf_mask(parts[0]);
      this.tipo = '1';
      return this.maskedId;
    }else if(parts[0].length == 14){
      this.maskedId = this.cnpj(parts[0]);
      this.tipo = '2';
      return this.maskedId;
    }
    else {
      this.presentLoading()
      return null;
    }

  }

unFormat(val) {
    if (!val) {
        return '';
    }
    val = val.replace(/\D/g, '');

    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
};

 cpf_mask(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
    
}

 cnpj(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}

// Validando o Telefone //
formatTel(valString) {
  if (!valString) {
      return '';
  }
  let val = valString.toString();
  const parts = this.telFormat(val).split(this.DECIMAL_SEPARATOR);
  this.pureResult = parts;
  if(parts[0].length >= 8){
    this.maskedId = this.tel_mask(parts[0]);
    return this.maskedId;
  }else{
    this.presentLoading()
    return null;
  }
};

telFormat(val) {
  if (!val) {
      return '';
  }
  val = val.replace(/\D/g, '');

  if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
  } else {
      return val.replace(/\./g, '');
  }
};

tel_mask(v) {
  v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
  v = v.replace(/(\d{0})(\d)/, '$1($2'); //Coloca um ponto entre o terceiro e o quarto dígitos
  v = v.replace(/(\d{2})(\d)/, '$1)$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
  return v;
  
}

// Buscando informações do CEP //
buscar(Nocep:string){
  return this.http
      .get(`https://viacep.com.br/ws/${Nocep}/json/`)
      .subscribe(data => this.resultado = this.converterRespostaParaCep(data));

}

// Convertendo formato Json //
public converterRespostaParaCep(cepNaResposta){
  
  this.logradouro = cepNaResposta.logradouro;
  this.bairro = cepNaResposta.bairro;
  this.complemento = cepNaResposta.complemento;
 
  return cepNaResposta;
}


// Validando o CEP //
formatCep(valString) {
  if (!valString) {
      return '';
  }
  let val = valString.toString();
  const parts = this.cepFormat(val).split(this.DECIMAL_SEPARATOR);
  this.pureResult = parts;
  if(parts[0].length == 8){
    // Fazendo mascara para o cep //
    this.maskedId = this.cep_mask(parts[0]);
    // Iniciando metodo de busca de cep //
    this.buscar(this.cep);
    return this.maskedId;
  }else{
    this.presentLoading()
    return null;
  }
};

cepFormat(val) {
  if (!val) {
      return '';
  }
  val = val.replace(/\D/g, '');

  if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
  } else {
      return val.replace(/\./g, '');
  }
};

cep_mask(v) {
  v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
  v = v.replace(/(\d{5})(\d)/, '$1-$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
  return v;
  
}



}
