import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ForgotDTO } from '../../models/forgot.dto';
import { AuthService } from './../../services/auth.service';

// Recuperando senha do usuario //

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {

  formGroup: FormGroup;

  // Variaveis de validacao //
  cpfOuCnpj = '';
  email = '';
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";
  pureResult: any;
  maskedId: any;
  val: any;
  v: any;

  // Declarando uma credencial para login viazia //
  creds : ForgotDTO = {

    email: '',
    cpfOuCnpj: ''

  };

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public formBuilder: FormBuilder,
    public http:HttpClient,
    public alertCtrl: AlertController,
    public auth: AuthService,
    public loadingCtrl: LoadingController) {

    // Instanciando um formBuilder responsavel pelo group //
    this.formGroup = this.formBuilder.group(
      {
        // Fazendo a validação dos campos //
        email:['',[Validators.required, Validators.email, Validators.minLength(8), Validators.maxLength(120)]],
        cpfOuCnpj:['',[Validators.required, Validators.maxLength(18)]],
            
      });


  }

  // Recuperando senha //
// Mostrando alerta de sucesso //
showInsertOk(){
  
  let alert = this.alertCtrl.create({
    title: 'Sucesso!',
    message: 'Senha enviada com sucesso!',
    enableBackdropDismiss: false,
    buttons: [
      {
      text: 'Ok',
      handler: () => {
        this.navCtrl.setRoot('HomePage');
      }
      }
    ]
  });
  alert.present();

}

// Voltando a tela inical //
voltarInicio(){
  let loader = this.presentLoading();
  let alert = this.alertCtrl.create({
    title: 'Ola!',
    message: 'Deseja voltar ao incio?',
    enableBackdropDismiss: false,
    buttons: [
      {
      text: 'Sim',
      handler: () => {
        this.navCtrl.setRoot("HomePage");
        loader.dismiss();
      }
    },
      {
        text: 'Não',
        handler: () => {
        }
      }
    ]
  });
  alert.present();

}

// Criando apresentação de erro personalizado //
presentLoading() {
  const loader = this.loadingCtrl.create({
  spinner: 'dots',
  content: "Aguarde!!!",   
});
   loader.present();
   return loader;
}

// Criando apresentação de erro personalizado //
forgotError() {
  const loader = this.loadingCtrl.create({
  spinner: 'dots',
  content: "E-mail ou CPF invalidos!",
  duration: 2600,
 
});
loader.present();

}

  forgotPass(){
  
  let loader = this.presentLoading();
  // Enviado informações de login //
  this.auth.forgotPassword(this.creds)
    .subscribe(response => {
    this.showInsertOk();
    // O dismiss encerra o loading //
    loader.dismiss();
  
  },
  error => {
    this.forgotError();    
  });
    
  }

// Criando apresentação de erro personalizado //
invalidLoading() {
  const loader = this.loadingCtrl.create({
  spinner: 'dots',
  content: "Valor digitado invalido!",
  duration: 1500,
 
});
loader.present();

}


  // Validando e formatando CPF e CNPJ //
  // @autor - TomCosta //
  // Link -> https://forum.ionicframework.com/t/cpf-cnpj-input-mask/109595 //
  format(valString) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    this.pureResult = parts;
    if(parts[0].length == 11){
      this.maskedId = this.cpf_mask(parts[0]);
      return this.maskedId;
    }else if(parts[0].length == 14){
      this.maskedId = this.cnpj(parts[0]);
      return this.maskedId;
    }
    else {
      this.invalidLoading();
      return null;
    }

  }

unFormat(val) {
    if (!val) {
        return '';
    }
    val = val.replace(/\D/g, '');

    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
};

 cpf_mask(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
    
}

 cnpj(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}


}