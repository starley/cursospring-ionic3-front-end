import { API_CONFIG } from './../../config/api.config';
import { CategoriaDTO } from './../../models/categoria.dto';
import { CategoriaService } from './../../services/domain/categoria.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {

  // Retornando a url de acesso //
  bucketUrl : string = API_CONFIG.bucketBaseUrl;
  // Lista para ser exposta para o controlador //
  itens : CategoriaDTO[];
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public categoriaService: CategoriaService) {
  }

  ionViewDidLoad() {
    // Chamando o metodo //
    this.categoriaService.findAll()
      .subscribe(response => {
        this.itens = response;
      },
      // Caso haja um erro na execução - vide error-interception.ts//
      error => {});
    }

    // Metodo para chamar a pagina de produtos //
    showProdutos(categoria_id: string){
      // Para passar um valor de uma pagina para outra, incluir um metodo 
      // "{variavel : objeto}" com dado em formato de objeto //
      this.navCtrl.push('ProdutosPage', {categoria_id: categoria_id});
    }

}
