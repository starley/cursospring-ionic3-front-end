import { AuthService } from './../../services/auth.service';
import { CredenciaisDTO } from './../../models/credenciais.dto';
import { Component } from '@angular/core';
import { LoadingController, NavController} from 'ionic-angular';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // Variavel para validação de campo //
  message: string;

  // Declarando uma credencial para login viazia //
  creds : CredenciaisDTO = {

    email: "",
    senha: ""

  };

  nomeButton : string;

  constructor(public loadingCtrl: LoadingController, 
    public navCtrl: NavController, 
    public menu: MenuController,
    public auth: AuthService){ }

    
  // Desabilitando menu Home na aplicação //

  ionViewWillEnter(){
   this.menu.swipeEnable(false);
  }

  ionViewDidLeave(){
   this.menu.swipeEnable(true);
  }

  // Manter token atualizado //
  ionViewDidEnter(){
  // Enviado informações de login //
  console.log("Token recarregado!");
  this.auth.refreshToken()
   .subscribe(response => {
    this.auth.successfulLogin(response.headers.get('Authorization'));
    this.navCtrl.setRoot('CategoriasPage');
    
},
   error => {});
  }

  // Criado loading //
  presentLoading() {
      const loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: "Bem-Vindo",

    });
    loader.present();
    return loader;
   
  }

  // Criado loading de erro //
  loadingErrorLogin() {
    const loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: "Usuáio ou Senha Invalidos!",
      duration: 3500,
    });
    loader.present();
  
  }

  // Criado loading de erro //
  emptyLogin() {
    const loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: this.message,
      duration: 1500,
    });
    loader.present();
  
  }

  // Metodo para chamar o login //
 fazendoLogin(){
  if (this.creds.email.length  == 0 ){
    this.message = "Digite o Usuário"
    this.emptyLogin();
  }
  else if(this.creds.senha.length == 0) {
    this.message = "Digite a Senha"
    this.emptyLogin();
  }
  else {
  // Enviado informações de login //
  let loader = this.presentLoading();
  this.auth.autenticate(this.creds).subscribe(response => {
    this.auth.successfulLogin(response.headers.get('Authorization'));

    // O dismiss encerra o loading //
    loader.dismiss();
    
    // Abrindo pagina apos o login //
    this.navCtrl.setRoot('CategoriasPage');

},
   error => {
    // O dismiss encerra o loading //
    loader.dismiss();
     this.loadingErrorLogin(); 
     
   });
  
  }
}

  // Metodo para chamar o cadastro //
  fazendoCadastro(){
    let loader = this.presentLoading();
    // Abrindo pagina apos o login //
    this.navCtrl.push('SignupPage');

    loader.dismiss();
    
  }

  // Metodo para recuperar senha //
  forgotPage(){
    let loader = this.presentLoading();
    // Abrindo pagina apos o login //
    this.navCtrl.push('ForgotPage');

    loader.dismiss();
  }

}
