import { PedidoService } from './../../services/domain/pedido.service';
import { ClienteService } from './../../services/domain/cliente.service';
import { ClienteDTO } from './../../models/cliente.dto';
import { CartService } from './../../services/domain/cart.service';
import { CartItem } from './../../models/cart-item';
import { PedidoDTO } from './../../models/pedido.dto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { EnderecoDTO } from '../../models/endereco.dto';

@IonicPage()
@Component({
  selector: 'page-order-confirmation',
  templateUrl: 'order-confirmation.html',
})
export class OrderConfirmationPage {

  // Declarando objeto 
  pedido: PedidoDTO;
  cartItem: CartItem[];
  cliente: ClienteDTO;
  endereco: EnderecoDTO;
  codePedido: string;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public cartService: CartService,
    public clienteService: ClienteService,
    public pedidoService: PedidoService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

    // Recebendo o pedido
    this.pedido = this.navParams.get('pedido');
  }

  ionViewDidLoad() {
   
    this.cartItem = this.cartService.getCart().items;

     // Buscando o cliente por id
     this.clienteService.findById(this.pedido.cliente.id)
      .subscribe(response => {
        // Usando um casting para carregar os dados do cliente //
        this.cliente = response as ClienteDTO;

        // response recebe a lista no campo entre chaves
        this.endereco = this.findEndereco(this.pedido.enderecoDeEntrega.id, response['enderecos']);
      },
      error => {
        this.navCtrl.setRoot('HomePage');
      })

  }

  // Função auxiliar para buscar o endereco
  private findEndereco(id: string, list: EnderecoDTO[]) : EnderecoDTO {

    // Procurando o id na lista de enderecos //
    let position = list.findIndex(x => x.id == id);
    return list[position];
  }

  // Pegando o valor total do carrinho para mostrar na tela 
  total(){

    return this.cartService.total();

  }

  // Enviando os dados para insersão no banco de dados
  checkout(){

    // Perguntando se quer finalizar a compra //
    let alert = this.alertCtrl.create({
      title: 'Confirmar pedido?',
      enableBackdropDismiss: false,
      buttons: [
        {
        text: 'Sim',
        handler: () => {

          // Ao realizar a confimação do pedido e enviado...
          let loader = this.enviandoPedido();
          this.pedidoService.insert(this.pedido)
            .subscribe(response =>{
              this.cartService.createOrClearCart();
              // Extraindo o Id da url do pedido salvo 
              this.codePedido = this.extractId(response.headers.get('location'));
              // Encerrando o loader
              loader.dismiss();
              this.pedidoConfirmado();
          },
          error => {
           if (error.status == 403){
           this.navCtrl.setRoot('HomePage');
          }
          });

        }
        },
        {
        text: 'Não',
        handler: () => {
          }
          }
      ]
      
    });
    alert.present();
  
  }

  // Voltando ao carrinho 
  back(){
    this.navCtrl.setRoot('CartPage');
  }

  // Mensagem de pedido confirmado
  pedidoConfirmado(){
    const loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: "Pedido enviado com sucesso!",
      duration: 2500,
    });
    loader.present();

  }

  enviandoPedido(){
    const loader = this.loadingCtrl.create({
      spinner: 'dots',
      content: "Aguarde!",
    });
    loader.present();
    return loader;

  }

  // Extraindo Id do pedido
  private extractId(location : string) : string{
    // Irá pegar a posição da ultima barra.
    let position = location.lastIndexOf('/');
    // Retonar a posição
    return location.substring(position + 1, location.length);
  }

  // Voltando para categorias //
  home(){
    this.navCtrl.setRoot('CategoriasPage');
  }

}
