import { Camera, CameraOptions } from '@ionic-native/camera';
import { ClienteService } from '../../services/domain/cliente.service';
import { ClienteDTO } from '../../models/cliente.dto';
import { StorageService } from '../../services/storage.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { API_CONFIG } from '../../config/api.config';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  // Variaveis de validacao //
  cpfOuCnpj = '';
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";
  pureResult: any;
  maskedId: any;
  val: any;
  v: any;

  cliente : ClienteDTO;
  picture: string;
  cameraOn: boolean = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public storage: StorageService,
              public clienteService: ClienteService,
              public camera: Camera) {
  }

  ionViewDidLoad() {

    // Pegando o valor do storage 
    let localUser = this.storage.getLocalUser();
    if ( localUser && localUser.email){
      this.clienteService.findByEmail(localUser.email)
      .subscribe(response => {
        // Usando um casting para o cliente
        this.cliente = response as ClienteDTO;
        
      // Buscar imagem
      this.getImageIfExist();
    },
    error => {
      // Tratamento de erro 403 "Não autorizado" 
      if (error.status == 403){
        this.navCtrl.setRoot('HomePage');
      }
    });
    
  }
  // Caso não consiga obter o localUser //
  else {
    this.navCtrl.setRoot('HomePage'); 
  }
} 
  // Metodo para verificar se a imagem existe 
  getImageIfExist(){
    this.clienteService.getImageFromBucket(this.cliente['id']).subscribe(reponse => { 
      this.cliente['imageUrl'] = `${API_CONFIG.bucketBaseUrl}/cp${this.cliente['id']}.jpg`;
    },
    error => {})
  }

  // Metodo para capturar foto
  getCameraPicture(){

    this.cameraOn = true;

    const options: CameraOptions ={
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) =>{
      this.picture = 'data:image/png;base64,' + imageData;
      this.cameraOn = false;
    }, (err) => {});
  }


  // Formatando CPF ou CNPJ //
  // Validando e formatando CPF e CNPJ //
  // @autor - TomCosta //
  // Link -> https://forum.ionicframework.com/t/cpf-cnpj-input-mask/109595 //
  format(valString) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    this.pureResult = parts;
    if(parts[0].length == 11){
      this.maskedId = this.cpf_mask(parts[0]);
      return this.maskedId;
    }else if(parts[0].length == 14){
      this.maskedId = this.cnpj(parts[0]);
      return this.maskedId;
    }
    else {
      return null;
    }

  }

unFormat(val) {
    if (!val) {
        return '';
    }
    val = val.replace(/\D/g, '');

    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
};

 cpf_mask(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
    
}

 cnpj(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}

// Validando o Telefone //
formatTel(valString) {
  if (!valString) {
      return '';
  }
  let val = valString.toString();
  const parts = this.telFormat(val).split(this.DECIMAL_SEPARATOR);
  this.pureResult = parts;
  if(parts[0].length >= 8){
    this.maskedId = this.tel_mask(parts[0]);
    return this.maskedId;
  }else{
  //  this.presentLoading()
    return null;
  }
};

telFormat(val) {
  if (!val) {
      return '';
  }
  val = val.replace(/\D/g, '');

  if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
  } else {
      return val.replace(/\./g, '');
  }
};

tel_mask(v) {
  v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
  v = v.replace(/(\d{0})(\d)/, '$1($2'); //Coloca um ponto entre o terceiro e o quarto dígitos
  v = v.replace(/(\d{2})(\d)/, '$1) $2'); //Coloca um ponto entre o terceiro e o quarto dígitos

  return v;
  
}

}