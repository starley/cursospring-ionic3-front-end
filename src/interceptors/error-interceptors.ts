import { FieldMessage } from '../models/fieldmessage';
import { StatusBar } from '@ionic-native/status-bar';
import { StorageService } from './../services/storage.service';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { AlertCmp, AlertController } from 'ionic-angular';

// Classe responsavel pelo tratamento de erros... 
// Versão 1.1 04/12/18

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(public storage: StorageService, public alertCtrl: AlertController){

    }

    // Retornando a requisição //
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //console.log("Passou no tratamento de erro Interceptor!");
        return next.handle(req)
        .catch((error, caugth) =>{

        // Criando variavel para retorno de erro //
        let errorObj = error;
        if (errorObj.error){
            errorObj = errorObj.error;
        }
        // Retornando em formato JSON //
        if (!errorObj.status){
            errorObj = JSON.parse(errorObj);
        }
        // Imprimindo o erro //
        console.log("Interceptor encontrou o seguinte erro!");
        console.log(errorObj);

        // Tratamento de erros //
        switch(errorObj.status){

            // Tratando erro 400 //
            case 400:
            this.handle400(errorObj);
            break;

            /* Tratando erro 401 - Já esta sendo tratado no Home.ts
            case 401:
            this.handle401();
            break;
            */

            // Tratando erro 403
            case 403:
            this.handle403();
            break;

            case 404:
            this.handle404();
            break;

            // Tratando erro 422
            case 422:
            this.handle422(errorObj);
            break;

          //  case 500:
          //  this.hendle500(errorObj);
          //  break;

            default:
            this.handleDefaultError(errorObj);
        }


            return Observable.throw(errorObj);
        }) as any;
    }

    handle400(errorObj){
        let alert = this.alertCtrl.create({
            title: 'Ops!',
            message: this.listErrors(errorObj.errors),
            enableBackdropDismiss: false,
            buttons: [
                {
                     text: 'Ok'
                }
                ]
            });
            alert.present();
     }

    // Tratamento de erro 403 "Não autorizado" 
    handle403(){
        this.storage.setLocalUser(null);
    }

    // Tratando de erro 404 "Não pode encontrar o que foi solicitado"
    handle404(){
        let alert = this.alertCtrl.create({
            title: 'Erro 404 - Não encontramos!',
            message: 'Não indentificamos os dados recebidos!',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        // Ira chamar o metodo //
        alert.present();
    }

    handle422(errorObj) {
        let alert = this.alertCtrl.create({
            title: 'Erro 422: Validação',
            message: this.listErrors(errorObj.errors),
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        alert.present();
    }


    /* Tratamento de erro 500 Erro no Banco
    hendle500(errorObj){
        let alert = this.aletCtrl.create({
            title: 'Ops!',
            message: 'Já existe um cadastro com os dados informados!',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        // Ira chamar o metodo //
        alert.present();
    }

    /* Tratamento de erro 401 "Falha de autenticação"
    hendle401(){
        let alert = this.arletCtrl.create({
            title: 'Erro 401: Falha de autenticação!',
            message: 'Usuário ou senha invalidos!',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        // Ira chamar o metodo //
        alert.present();
    }
    */

    // Tratamento de erros gerais 
    handleDefaultError(errorObj){
        let alert = this.alertCtrl.create({
            title: 'Erro '+ errorObj.status +': '+ errorObj.error,
            message: errorObj.message,
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Ok'
                }
            ]
        });
        alert.present();
    }

 // Criando lista de erros 

    private listErrors(messages: FieldMessage[]): string{
      let s : string = '';
      for (var i=0; i<messages.length; i++){
          s = s + '<p><strong>'+ messages[i].fieldName + "</strong>:" + messages[i].message + '</p>';
      }  
      return s;
    }
}

    export const ErrorInterceptorProvider = { 
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorInterceptor,
        multi: true,
    };