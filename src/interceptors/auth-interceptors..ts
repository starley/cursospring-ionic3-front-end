import { API_CONFIG } from './../config/api.config';
import { StorageService } from '../services/storage.service';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

// Classe responsavel pelo tratamento de autorização... 
// Versão 1.1 04/12/18

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
   
    constructor(public storage: StorageService) {
    }

    // Retornando a requisição //
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       
        let localUser = this.storage.getLocalUser();

        // Configurando variavel para verficar requisição para API //
        let N = API_CONFIG.baseUrl.length;
        let requestToApi = req.url.substring(0, N) == API_CONFIG.baseUrl;

        if (localUser && requestToApi){
            // Clonando a autorização //
            const authReq = req.clone({headers: req.headers.set('Authorization', 'Bearer ' + localUser.token)});
            return next.handle(authReq);
        }
        else {
             return next.handle(req)
        }
    }

}

export const AuthInterceptorProvider = { 
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
};