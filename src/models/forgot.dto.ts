// Interface para recuperação de senha //
export interface ForgotDTO{

    email: string;
    cpfOuCnpj: string;

}