import { EstadoDTO } from "./estado.dto";

// Cidade DTO /
export interface CidadeDTO{

    id: string;
    nome: string;
    estado? : EstadoDTO;
    
}