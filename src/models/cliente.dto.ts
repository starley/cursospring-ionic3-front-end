export interface ClienteDTO{

    // Dados que irão ser utilizados pelo cliente na tela de perfil //
    id: string;
    nome: string;
    email: string;
    telefones: string;
    cpfOuCnpj: string;
    tipo: string;
    imageUrl?: string;
}