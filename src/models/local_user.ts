export interface LocalUser{

    // Armazenado token //
    token: string;
    // Armazenando email //
    email: string;
}