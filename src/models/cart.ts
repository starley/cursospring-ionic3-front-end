import { CartItem } from './cart-item';

export interface Cart {
    // Recebendo os itens do carrinho //
    items: CartItem[];
}