
// Criado classe de interface produtos //
export interface ProdutoDTO {

    id: string;
    nome: string;
    preco: number;
    imageUrl?: string;
}