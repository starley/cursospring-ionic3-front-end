import { CidadeDTO } from './cidade.dto';

export interface EnderecoDTO{

    id : string;
    bairro : string;
    cep : string;
    complemento : string;
    logradouro : string;
    numero : string;
    cidade : CidadeDTO;
}