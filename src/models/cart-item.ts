import { ProdutoDTO } from './produto.dto';

// Carrinho de compras //

export interface CartItem {
    // Quantidade de itens do carrinho //
    quantidade: number;
    // Produto selecionado //
    produto: ProdutoDTO;
}