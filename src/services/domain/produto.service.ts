import { ProdutoDTO } from '../../models/produto.dto';
import { Observable } from 'rxjs/Rx';
import { API_CONFIG } from './../../config/api.config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";


@Injectable()
export class ProdutoService{

    constructor(public http: HttpClient){

    }

    // Metodo de retorno de produtos por categoria //
        // Refatoração do metodo para infinit scroll //
    findByCategoria(categoria_id : string, page: number = 0, linesPerPage: number = 24){
        return this.http.get(`${API_CONFIG.baseUrl}/produtos/?categorias=${categoria_id}&page=${page}&linesPerPage=${linesPerPage}`);
    }

    // Pegando imagem pequena //
    getSmallImageFromBucket(id : string): Observable<any> {
        let url = `${API_CONFIG.bucketBaseUrl}/prod${id}-small.jpg`
        return this.http.get(url, {responseType : 'blob'});
    }

    // Metodo para encontrar o produto por ID //
    findById(produto_id : string){
        return this.http.get<ProdutoDTO>(`${API_CONFIG.baseUrl}/produtos/${produto_id}`);
    }

    // Pegando imagem grande //
    getImageFromBucket(id : string): Observable<any> {
        let url = `${API_CONFIG.bucketBaseUrl}/prod${id}.jpg`
        return this.http.get(url, {responseType : 'blob'});
    }

}