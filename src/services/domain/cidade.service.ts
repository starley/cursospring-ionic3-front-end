import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../../config/api.config";
import { Observable } from "rxjs/Rx";
import { CidadeDTO } from "../../models/cidade.dto";

// Cidade de serviços //
@Injectable()
export class CidadeService {

    constructor(public http: HttpClient){
    }

    // Metodo de retorno das cidades de um estado
    // Será atraves do id do estado a pupulação das cidades //
    findAll(estado_id: string) : Observable<CidadeDTO[]> {
        // Função tipada com retorno de uma lista //
        return this.http.get<CidadeDTO[]>(`${API_CONFIG.baseUrl}/estados/${estado_id}/cidades`);
    }

}