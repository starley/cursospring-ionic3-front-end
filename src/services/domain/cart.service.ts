import { Cart } from '../../models/cart';
import { ProdutoDTO } from '../../models/produto.dto';
import { StorageService } from '../storage.service';
import { Injectable } from "@angular/core";

@Injectable()
export class CartService{

    constructor(public storage: StorageService){

    }

    // Metodo para limpar ou criar o carrinho //
    createOrClearCart() : Cart {
        let cart: Cart = {items: []};
        this.storage.setCart(cart);
        return cart;
    }

    // Pegando o carrinho 
    getCart(): Cart{
        let cart: Cart = this.storage.getCart();
        // Verificando se o carrinho está nulo //
        if (cart == null){
            cart = this.createOrClearCart();
        }
        return cart;
    }

    // Adicionando produto ao carrinho 
    addProduto(produto: ProdutoDTO) : Cart{

        let cart = this.getCart();
        // FindIndex precisa de um predicado. Para encontrar a posição de um elemento //
        let position = cart.items.findIndex(x => x.produto.id == produto.id);
        // Se o produto não existir "usa-se -1"
        if(position == -1){
            // Caso não tenha ele ira adicionar o produto ao carrinho
            cart.items.push({quantidade: 1, produto: produto});
        }
        // Atualizar o carrinho no localStorage
        this.storage.setCart(cart);
        return cart;
    }

    // Removendo produto do carrinho
    removeProduto(produto: ProdutoDTO) : Cart{

        let cart = this.getCart();
            // FindIndex precisa de um predicado. Para encontrar a posição de um elemento //
        let position = cart.items.findIndex(x => x.produto.id == produto.id);
            // Se o produto existir
        if(position != -1){
            // Removendo item do carrinho 
            cart.items.splice(position, 1);
        }
            // Atualizar o carrinho no localStorage
        this.storage.setCart(cart);
        return cart;
    }

    // Incrementando a quantidade de produto no carrinho
    increaseQuantity(produto: ProdutoDTO) : Cart{

        let cart = this.getCart();
            // FindIndex precisa de um predicado. Para encontrar a posição de um elemento //
        let position = cart.items.findIndex(x => x.produto.id == produto.id);
            // Se o produto existir
         if(position != -1){
            // Removendo um item do carrinho 
            cart.items[position].quantidade++;
        }
            // Atualizar o carrinho no localStorage
        this.storage.setCart(cart);
        return cart;
    }

    // Desincrementando a quantidade de produto no carrinho
    decreaseQuantity(produto: ProdutoDTO) : Cart{

        let cart = this.getCart();
            // FindIndex precisa de um predicado. Para encontrar a posição de um elemento //
        let position = cart.items.findIndex(x => x.produto.id == produto.id);
            // Se o produto existir
         if(position != -1){
            // Removendo um item do carrinho 
            cart.items[position].quantidade--;
            // Caso o carinho esteja com quantidade 0 o produto será removido //
            if(cart.items[position].quantidade < 1){
                cart = this.removeProduto(produto);
            }
        }
            // Atualizar o carrinho no localStorage
        this.storage.setCart(cart);
        return cart;
    }

    // Valor total do carrinho //
    total() : number{
        let cart = this.getCart();
        let sum = 0;
        for(var i=0; i<cart.items.length; i++){
            sum += cart.items[i].produto.preco * cart.items[i].quantidade;
         
        }
        return sum;
    }

    // Quantidade de itens no carrinho  //
    quatidadeItem() : number{
        let cart = this.getCart();
        let qut = 0;
        for(var i=0; i<cart.items.length; i++){
            qut += cart.items[i].quantidade;
        }
        return qut;
    }

}