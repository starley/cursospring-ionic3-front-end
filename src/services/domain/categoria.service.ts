import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../../config/api.config";
import { Observable } from "rxjs/Rx";
import { CategoriaDTO } from "../../models/categoria.dto";

// Categoria de serviços //
@Injectable()
export class CategoriaService {

    constructor(public http: HttpClient){
    }

    // Metodo de retorno das categorias //
    findAll() : Observable<CategoriaDTO[]> {
        // Função tipada com retorno de uma lista //
        return this.http.get<CategoriaDTO[]>(`${API_CONFIG.baseUrl}/categorias`);
    }

}