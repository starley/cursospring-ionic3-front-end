import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../../config/api.config";
import { Observable } from "rxjs/Rx";
import { EstadoDTO } from "../../models/estado.dto";

// Cidade de serviços //
@Injectable()
export class EstadoService {

    constructor(public http: HttpClient){
    }

    // Metodo de retorno dos estado
    findAll() : Observable<EstadoDTO[]> {
        // Função tipada com retorno de uma lista //
        return this.http.get<EstadoDTO[]>(`${API_CONFIG.baseUrl}/estados`);
    }

}