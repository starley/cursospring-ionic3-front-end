import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { API_CONFIG } from '../../config/api.config';
import { PedidoDTO } from '../../models/pedido.dto';

@Injectable()
// Classe responsavel pela resposta do pedido //
export class PedidoService{

    constructor (public http: HttpClient){
    }

    // Metodo para inserir o pedido //
    insert(obj: PedidoDTO){
        return this.http.post(
            `${API_CONFIG.baseUrl}/pedidos`,
            obj,
            {
                observe: 'response',
                responseType: 'text'
            }
        );
    }

}