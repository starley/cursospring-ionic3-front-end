import { StorageService } from './../storage.service';
import { LocalUser } from './../../models/local_user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Rx';
import { ClienteDTO } from '../../models/cliente.dto';
import { API_CONFIG } from '../../config/api.config';
import { tokenKey } from '@angular/core/src/view';

@Injectable()
// Classe responsavel pela resposta do cliente //
export class ClienteService{

    constructor (public http: HttpClient,
                 public storage: StorageService ){
    }

    // Metodo para abusca do cliente atraves do Id //
    findById(id : string){

        // Retornando a requisicao do email //
        return this.http.get(
            `${API_CONFIG.baseUrl}/clientes/${id}`);

    }

    // Metodo para abusca do cliente atraves do email //
    findByEmail(email : string){

        // Retornando a requisicao do email //
        return this.http.get(
            `${API_CONFIG.baseUrl}/clientes/email?value=${email}`);

    }

    // Pegando a imagem do perfil do cliente //
    getImageFromBucket(id : string) : Observable<any> {
        let url = `${API_CONFIG.bucketBaseUrl}/cp${id}.jpg`
        return this.http.get(url, {responseType : 'blob'});
    }

    // Metodo para inserir o cliente //
    insert(obj : ClienteDTO) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/clientes`, 
            obj,
            { 
                observe: 'response', 
                responseType: 'text'
            }
        ); 
    }
}