import { Cart } from './../models/cart';
import { Injectable } from "@angular/core";
import { STORAGE_KEYS } from "../config/storage_keys.config";
import { LocalUser } from "../models/local_user";

// Servico reponsavel por pegar e retornar os dados de login //
@Injectable()
export class StorageService {

    // Recebendo os dados digitados 
    getLocalUser(): LocalUser {

        // Variavel 
        let usr = localStorage.getItem(STORAGE_KEYS.localUser);
        if ( usr == null){
            return null;
        }
        else {
            // Retornando em formato JSON
            return JSON.parse(usr);
        }


    }

    // Recebendo o valor e armazenar no Storage 
    setLocalUser(obj : LocalUser) {
        // Caso seja nulo ira limpar o localStorage
        if (obj == null){
            localStorage.removeItem(STORAGE_KEYS.localUser);
        }
        else {
            // Ira armazenar 
            // stringfy = converte o valor para string 
            localStorage.setItem(STORAGE_KEYS.localUser, JSON.stringify(obj));
        }
    }

    // Criando um localStorage para o carrinho //
    getCart() : Cart {

        let cart = localStorage.getItem(STORAGE_KEYS.cart);
        if (cart != null){
            return JSON.parse(cart);
        }
        else {
            // Retornando vazio 
            return null;
        }
    }

    // Recebendo o valor e armazenar no Storage 
    setCart(obj: Cart){
        if(obj != null){
            localStorage.setItem(STORAGE_KEYS.cart, JSON.stringify(obj));
        }
        else{
            localStorage.removeItem(STORAGE_KEYS.cart);
        }
    }
    
}