import { CartService } from './domain/cart.service';
import { LocalUser } from './../models/local_user';
import { API_CONFIG } from './../config/api.config';
import { HttpClient } from '@angular/common/http';
import { CredenciaisDTO } from './../models/credenciais.dto';
import { Injectable } from "@angular/core";
import { StorageService } from './storage.service';
import { JwtHelper } from 'angular2-jwt';
import { ForgotDTO } from '../models/forgot.dto';

// Service de autenticação de serviços //
@Injectable()
export class AuthService {

    // Serve para ajudar a extrair dados do Token //
    jwtHelper : JwtHelper = new JwtHelper();

    // Construtor //
    constructor( public http: HttpClient,
        public storage : StorageService,
        public cartService: CartService){

    }

    // Metodo para receber as credenciais de login //
    autenticate(creds : CredenciaisDTO){
        // Acessando a URL //
        return this.http.post(`${API_CONFIG.baseUrl}/login`,
        // Recebendo os dados do creds //
        creds,
        {
            // Pegando o header da resposta //
            observe: 'response',
            responseType: 'text'
        });
    }

     // Metodo para manter as credenciais //
     refreshToken(){
        // Acessando a URL //
        return this.http.post(
            `${API_CONFIG.baseUrl}/auth/refresh_token`,      
        {},
        {
            // Pegando o header da resposta //
            observe: 'response',
            responseType: 'text'
        });
    }


    // Caso o login tenha sucesso 
    successfulLogin(authorizationValue : string){

        // Recebendo a chave de autorizacao e fazendo a leiura dela apos o 7 caractere //
        let tok = authorizationValue.substring(7); 
        // Variavel para armazenar o token do usuario 
        let usr : LocalUser = {
            token : tok,
            email : this.jwtHelper.decodeToken(tok).sub
        };

        this.storage.setLocalUser(usr);

        // Limpando o carrinho de compras ao realizar o login //
        this.cartService.createOrClearCart();

    }

    // Metodo para sair - remove o usuario do localStorage //
    logout(){
        this.storage.setLocalUser(null);
    }

    // Metodo para receber as credenciais de login //
    forgotPassword(creds : ForgotDTO){
        // Acessando a URL //
        return this.http.post(`${API_CONFIG.baseUrl}/auth/forgot/`,
        // Recebendo os dados do creds //
        creds,
        {
            // Pegando o header da resposta //
            observe: 'response',
            responseType: 'text'
        });
    }

}